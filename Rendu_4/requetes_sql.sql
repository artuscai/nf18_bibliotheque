-- Intertions supplementaires pour rendre les requetes plus riches
-- !!! REMARQUE !!! Nous avons corrigé une erreur dans les tables
-- Donc si l'ancienne version est utilisée les insertions dans sanction ne fonctionnerons pas

INSERT INTO sanction (idadherent, datedebut, duree, typesanction) VALUES (1,CURRENT_DATE, 10, 'degradation');
INSERT INTO pret (code, ide, ida, datepret, duree) VALUES (6, 1, 1, CURRENT_DATE, 10);


-- Login:

-- Login adhérents :

SELECT idAdherent FROM CompteUtilisateur WHERE login = 'jeandupont' AND motDePasse = 'password123' AND idAdherent IS NOT NULL;

-- Pour ensuite récupérer le compte utilisateur associé :

SELECT * FROM Adherent WHERE id = '1' ;


--Login membre du personnel :

SELECT idPersonnel FROM CompteUtilisateur WHERE login = 'camilleroux' AND motDePasse = 'admin123' AND idPersonnel IS NOT NULL;

-- Pour ensuite récuperer le compte personnel associé
SELECT * FROM MembrePersonnel WHERE id = '1' ;



-- La partie pour les Ahérents

-- Voir la liste de toute les livres, filmes et oeuvres musicales respectivement triées par date d'apparition:
SELECT * FROM vLivre ORDER BY vlivre.dateapparition DESC;
SELECT * FROM vFilm ORDER BY  vfilm.dateapparition DESC;
SELECT * FROM vOeuvreMusicale ORDER BY voeuvremusicale.dateapparition DESC;


-- Rechercher une ressource par titre tout type confondu
-- REMARQUE  : On utilise LOWER() pour que les requetes ne dépendent pas de la case

SELECT * FROM ressource WHERE LOWER(ressource.titre) LIKE LOWER('%Requiem%');

-- Rechercher une ressource par titre dans livres, filmes et oeuvres musicales:

SELECT * FROM vlivre WHERE LOWER(vlivre.titre) LIKE LOWER('%Vingt%'); -- idem pour les autres types just a changer de vue


-- Recherche par rapport à un contributeur (affichage des plus récents d'abord) :


-- Livres

SELECT liv.code, liv.titre, liv.genre, liv.codeClassification, liv.resume, aut.nom, aut.prenom FROM
vLivre liv JOIN ContribueLivre cliv ON liv.code = cliv.codeLivre
JOIN vAuteur aut ON aut.id = cliv.idContributeur
WHERE LOWER(aut.nom) LIKE LOWER('%Ver%') AND LOWER(aut.prenom) LIKE LOWER('%J%')
ORDER BY liv.dateapparition DESC;

-- si on veut tout les auteurs d'un livre avec son id
SELECT aut.* FROM vAuteur aut JOIN ContribueLivre cliv ON aut.id = cliv.idContributeur WHERE cliv.codeLivre = '3'; -- tout les auteurs du livre "vingt mille lieux etc."

-- Filmes
SELECT
film.* FROM
vFilm film JOIN ContribueFilm cfilm ON film.code = cfilm.codeFilm
JOIN vRealisateur rea ON cfilm.idContributeur = rea.id
JOIN vActeur act ON cfilm.idContributeur = act.id
WHERE (LOWER(act.nom) LIKE LOWER('%Chaplin%') AND LOWER(act.prenom) LIKE LOWER('%Charlie%')) OR (LOWER(rea.nom) LIKE LOWER('%Chaplin%') AND LOWER(rea.prenom) LIKE LOWER('%Charlie%'))
ORDER BY film.dateapparition DESC;

-- voir tout les realisateurs d'un film avec les id
SELECT rea.* FROM vrealisateur rea JOIN contribuefilm cfilm ON rea.id = cfilm.idcontributeur WHERE cfilm.codefilm = '2'; -- realisateurs du film "Inception"
-- voir tout les acteurs d'un film
SELECT act.* FROM vacteur act JOIN contribuefilm cfilm ON act.id = cfilm.idcontributeur WHERE cfilm.codefilm = '5'; -- acteurs du film "Shindlers List"

-- Musiques
SELECT music.* FROM
    voeuvremusicale music JOIN contribuemusique cmusic ON music.code = cmusic.codemusic
    JOIN vinterprete inter ON cmusic.idcontributeur = inter.id
    JOIN vcompositeur compo ON compo.id = cmusic.idcontributeur
    WHERE  (LOWER(inter.nom) LIKE LOWER('%Mozart%') AND LOWER(inter.prenom) LIKE LOWER('%Wolfgang%')) OR (LOWER(compo.nom) LIKE LOWER('%Mozart%') AND LOWER(compo.prenom) = LOWER('%Wolfgang%'))
    ORDER BY music.dateapparition DESC;

-- voir tout les interpretes d'un morceau avec l'id
SELECT inter.* FROM vinterprete inter JOIN contribuemusique cmusic ON inter.id = cmusic.idcontributeur WHERE cmusic.codemusic = '4' ;-- tout les interpretes de requiem
-- voir tout les compositeurs d'un morceau avec l'id
SELECT compo.* FROM vcompositeur compo JOIN contribuemusique cmusic ON compo.id = cmusic.idcontributeur WHERE cmusic.codemusic = '4' ;-- tout les compositeurs de requiem


-- Voir tout ses prets
SELECT ressource.titre,pret.duree,pret.datepret,exemplaire.etat, (pret.datepret + pret.duree * INTERVAL '1 day') AS datefin FROM ressource
    JOIN exemplaire on ressource.code = exemplaire.code
    JOIN pret ON (pret.code = ressource.code) AND (exemplaire.id = pret.ide)
    WHERE pret.ida = '1';
    -- pour l'adhérent 1 => jean du pont


-- Voir le nombre d'exemplaires dispos pour une ressource
-- On prend tous les exemplaires qui n'ont pas encore été empruntés ou qui ne sont pas a
-- atuellement censée êtres empruntés
SELECT count(*) AS nb_dispo FROM exemplaire
    LEFT JOIN pret ON pret.code = exemplaire.code AND exemplaire.id = pret.ide
    WHERE exemplaire.code = '1' AND (pret.code = NULL OR NOT (CURRENT_DATE BETWEEN pret.datePret AND (pret.datePret + pret.duree * INTERVAL '1 day')));



 -- Membre Personnel :

-- Voir la liste de tous les membres du personnel :

SELECT id, nom, prenom, adresse, email
    FROM MembrePersonnel
    ORDER BY nom, prenom;

-- Détails d’un membre du personnel spécifique :

SELECT id, nom, prenom, adresse, email
    FROM MembrePersonnel
    WHERE id = 1;

-- Voir la liste de tous les adhérents avec leur statut (blacklisté ou non) :

SELECT id, nom, prenom, email, blackliste
FROM Adherent;

-- Voir la liste de tous les adhérents blacklistés :

SELECT id, nom, prenom, email
    FROM Adherent
    WHERE blackliste = TRUE;


-- Adhérents ayant un compte utilisateur :
SELECT a.id, a.nom, a.prenom, a.email, c.login
    FROM Adherent a
    JOIN CompteUtilisateur c ON a.id = c.idAdherent
    WHERE c.idAdherent IS NOT NULL;

-- Adhérents avec des prêts actifs :
SELECT a.id, a.nom, a.prenom, r.titre, p.datePret, (p.datePret + p.duree * INTERVAL '1 day') as date_fin
FROM Adherent a
JOIN Pret p ON a.id = p.idA
JOIN Ressource r ON p.code = r.code
WHERE (p.datePret + p.duree * INTERVAL '1 day')  > CURRENT_DATE;


-- Liste des membres du personnel avec leurs comptes utilisateur :

SELECT m.id, m.nom, m.prenom, m.adresse, m.email, c.login
    FROM MembrePersonnel m
    JOIN CompteUtilisateur c ON m.id = c.idPersonnel;

-- Liste des adhérents qui ont eu des sanctions :

SELECT a.id, a.nom, a.prenom, s.dateDebut, s.duree, s.typeSanction
FROM Adherent a
JOIN Sanction s ON a.id = s.idAdherent
ORDER BY s.dateDebut DESC;



-- Adhérents ayant emprunté le plus de ressources :

SELECT a.id, a.nom, a.prenom, COUNT(p.code) AS nbPrets
FROM Adherent a
JOIN Pret p ON a.id = p.idA
GROUP BY a.id, a.nom, a.prenom
ORDER BY nbPRets DESC;

-- Recherche d’un adhérent par id :

SELECT * FROM Adherent WHERE id = '1';

-- Recherche d’un adhérent par nom prenom :

SELECT * FROM Adherent WHERE LOWER(adherent.nom) = LOWER('dupont') AND LOWER(adherent.prenom) = LOWER('Jean');

-- Prêt, retard, sanction

-- Vérifie si une sanction est active pour l’adhérent 1

SELECT S.typeSanction, S.dateDebut, S.duree
FROM Sanction S
WHERE S.idAdherent = 1 AND CURRENT_DATE BETWEEN S.dateDebut AND (S.dateDebut + S.duree * INTERVAL '1 day');

-- Nombre de prêt en cours pour chaque adhérent
SELECT idA AS idAdherent, COUNT(*) AS nombrePretsEnCours
FROM Pret
WHERE  CURRENT_DATE BETWEEN datePret AND (datePret + duree * INTERVAL '1 day')
GROUP BY idA;

-- Liste l’ensemble des prêts de l’adhérent 1
SELECT R.code, R.titre, E.id AS idExemplaire, P.datePret, P.duree
FROM Pret P
JOIN Exemplaire E ON P.code = E.code AND P.idE = E.id
JOIN Ressource R ON E.code = R.code
WHERE P.idA = 1;


-- Vérifie les ressources qui peuvent être empruntées
SELECT R.code, R.titre, E.etat
FROM Ressource R JOIN Exemplaire E ON R.code = E.code LEFT JOIN Pret P ON E.code = P.code AND E.id = P.idE
WHERE (P.idE IS NULL OR NOT (CURRENT_DATE BETWEEN P.datePret AND (P.datePret + P.duree * INTERVAL '1 day'))) AND (E.etat IN ('bon', 'neuf'));


-- Voir le nombre d'exemplaires dispos pour une ressource
-- On prend tous les exemplaires qui n'ont pas encore été empruntés ou qui ne sont pas
-- actuellement censée êtres empruntés et qui sont en état

SELECT count(*) AS nb_dispo FROM exemplaire
    LEFT JOIN pret ON pret.code = exemplaire.code AND exemplaire.id = pret.ide
    WHERE exemplaire.code = '1'
      AND
        (pret.code = NULL OR NOT (CURRENT_DATE BETWEEN pret.datePret AND (pret.datePret + pret.duree * INTERVAL '1 day')))
      AND (exemplaire.etat IN ('bon', 'neuf'));



-- Gestion des statistiques :

-- Nombre total d’adhérents, de membres du personnel :

SELECT
	(SELECT COUNT(*) FROM Adherent) AS nbAdherents,
    (SELECT COUNT(*) FROM MembrePersonnel) AS nbPersonnel;

-- Lister les documents les plus empruntés (dans la limite de 10 documents) :

SELECT CASE
        WHEN R.code IN (SELECT codeLivre FROM Livre) THEN 'Livre'
        WHEN R.code IN (SELECT codeFilm FROM Film) THEN 'Film'
        WHEN R.code IN (SELECT codeMusique FROM OeuvreMusicale) THEN 'Musique'
    END AS type_document,
    R.titre,
    COUNT(P.code) AS nombre_emprunts
FROM Ressource AS R
JOIN Exemplaire AS E ON R.code = E.code
JOIN Pret AS P ON E.code = P.code AND E.id = P.idE
GROUP BY R.titre, type_document ORDER BY nombre_emprunts DESC
LIMIT 10;



-- Lister les types de ressources les plus populaires :

SELECT CASE
        WHEN R.code IN (SELECT codeLivre FROM Livre) THEN 'Livre'
        WHEN R.code IN (SELECT codeFilm FROM Film) THEN 'Film'
        WHEN R.code IN (SELECT codeMusique FROM OeuvreMusicale) THEN 'Musique'
    END AS type_document,
    COUNT(P.code) AS nombre_emprunts
FROM Ressource AS R
JOIN Exemplaire AS E ON R.code = E.code
JOIN Pret AS P ON E.code = P.code AND E.id = P.idE
GROUP BY type_document ORDER BY nombre_emprunts DESC;


-- Lister les documents les plus empruntés en fonction du genre (dans la limite de 10 documents)  :

-- Livre :

SELECT L.titre, COUNT(P.code) AS nombre_emprunts
FROM vLivre AS L JOIN Exemplaire AS E ON L.code = E.code
JOIN Pret AS P ON E.code = P.code AND E.id = P.idE
GROUP BY L.titre ORDER BY nombre_emprunts DESC
LIMIT 10;

-- Film :

SELECT F.titre, COUNT(P.code) AS nombre_emprunts
FROM vFilm AS F JOIN Exemplaire AS E ON F.code = E.code
JOIN Pret AS P ON E.code = P.code AND E.id = P.idE
GROUP BY F.titre ORDER BY nombre_emprunts DESC
LIMIT 10;

-- Oeuvre Musicale :

SELECT M.titre, COUNT(P.code) AS nombre_emprunts
FROM vOeuvreMusicale AS M JOIN Exemplaire AS E ON M.code = E.code
JOIN Pret AS P ON E.code = P.code AND E.id = P.idE
GROUP BY M.titre ORDER BY nombre_emprunts DESC
LIMIT 10;


-- Lister les documents ayant la plus haute moyenne de durée d’emprunts (dans la limite de 10 documents) :

SELECT CASE
        WHEN R.code IN (SELECT codeLivre FROM Livre) THEN 'Livre'
        WHEN R.code IN (SELECT codeFilm FROM Film) THEN 'Film'
        WHEN R.code IN (SELECT codeMusique FROM OeuvreMusicale) THEN 'Musique'
    END AS type_document,
    R.titre,
    AVG(P.duree) AS duree_moyenne
FROM Ressource AS R
JOIN Exemplaire AS E ON R.code = E.code
JOIN Pret AS P ON E.code = P.code AND E.id = P.idE
GROUP BY type_document, R.titre ORDER BY duree_moyenne DESC
LIMIT 10;

-- Lister les noms et prénoms des adhérents ET leur type de documents préféré (en se basant sur le plus de prêt du type de document en question) :

SELECT A.nom, A.prenom, CASE
         WHEN R.code IN (SELECT codeLivre FROM Livre) THEN 'Livre'
         WHEN R.code IN (SELECT codeFilm FROM Film) THEN 'Film'
         WHEN R.code IN (SELECT codeMusique FROM OeuvreMusicale) THEN 'Musique'
       END AS type_document,
       COUNT(P.code) AS nombre_emprunts
FROM Adherent AS A
JOIN Pret AS P ON A.id = P.idA
JOIN Exemplaire AS E ON P.code = E.code AND P.idE = E.id
JOIN Ressource AS R ON E.code = R.code
GROUP BY A.nom, A.prenom, type_document ORDER BY nombre_emprunts DESC;

