import psycopg2 as sql
from datetime import datetime
import atexit

HOST = "tuxa.sme.utc"
DB = "dbnf18a002"
USER = "nf18a002"
PW = "tQ5HVQ0wxza2"


def login(curs):
    """retourne le couple (idAdherent, idPersonnel) dont un des membres et None en fonction du type de compte"""
    print("---------------------  LOGIN  ---------------------")
    username = input("username : ")
    password = input("password : ")
    req = "SELECT idAdherent, idPersonnel FROM CompteUtilisateur WHERE login = %s AND motDePasse = %s;"
    try:
        curs.execute(req, (username, password))
        res = curs.fetchone()
    except sql.Error as err:
        print("Erreur de BDD dans login: ", err)
        return None

    return res

def afficher_ressources(curs):
    """affiche toutes les ressources de la base de données"""
    req = "SELECT code, titre, codeclassification FROM ressource;"
    try:
        curs.execute(req)
        res = curs.fetchall()
        print(f"{'Code' : <25}|{'Titre' : ^40}|{'Code Classification' : >25}")
        for ressource in res:
            code, titre, codeClass = ressource
            print(f"{code : <25}|{titre : ^40}|{codeClass : >25}")
    except sql.DatabaseError as err:
        print("Erreur de BDD dans afficher_ressources : ", err)
        raise


def ajouter_nouvelle_adherent(curs, conn):
    """Ajout d'un adhérent"""
    id = input("id : ")
    nom = input("Nom : ")
    prenom = input("Prénom : ")
    email = input("Email : ")
    try:
        # ajout de l'adhérent
        req = """ INSERT INTO adherent (id, nom, prenom,email, blackliste) VALUES (%s, %s, %s, %s, %s); """
        curs.execute(req, (id, nom, prenom, email, False))

        # creation du compte
        creer_compte_adherent(curs, conn, id)
        conn.commit()
        print("Nouvel adhérent ajouté avec succès.")

    except sql.Error as err:
        print("Erreur lors de l'ajout de l'adhérent : ", err)
        conn.rollback()

def creer_compte_adherent(curs, conn, idAdherent):
    """Creation d'un compteutilisateur pour un adherent"""
    try:

        # on verifie qu'un compte n'existe pas deje pour cette adhérent
        curs.execute("SELECT * FROM compteutilisateur WHERE idadherent = %s", (idAdherent,))
        if (curs.fetchone()):
            print("Cette adhérent à déjà un compte utilisateur.")
            return None
        else :
            login = input("Nouveau login : ")
            password = input("Nouveau mot de passe : ")
            curs.execute("""
                INSERT INTO compteutilisateur (login, motdepasse, idadherent, idpersonnel) 
                VALUES (%s,%s,%s,NULL);
            """, (login, password, idAdherent))

    except sql.IntegrityError as err:
        print("Erreur dans les donées l'ors de la création du compte.")
        print("Il se peut qu'un adhérent ai le même login que celui fournit.")
        raise

    except sql.DatabaseError as err:
        print("Erreur de BDD dans creer_compte_utilisateur.")
        raise


def rechercher_adherent(curs):
    """fonction qui cherche un adherent, retourne un tuple (idAdherent : int, actif : bool)"""
    print("-------------- RECHERCHE D'UN ADHERENT -------------- ")
    nom = "%" + input("Nom : ") + "%"
    prenom = "%" + input("Prenom : ") + "%"
    req = """SELECT id, nom, prenom FROM adherent WHERE LOWER(nom) LIKE LOWER(%s) AND LOWER(prenom) LIKE LOWER(%s);"""
    curs.execute(req, (nom, prenom))
    res = curs.fetchall()
    if (len(res) == 0):
        print("Aucun adhérent de correspond a votre requete.")
        return None
    else:
        # affichage de tout les adherents trouvées pour que l'utilisateur en choisisse un
        print(f"{'Choix' : <25} | {'Nom' : ^20} | {'Prenom' : >25}")
        for index, info_adherent in enumerate(res):
            id, nom, prenom = info_adherent
            print(f"{index + 1 : <25} | {nom : ^20} | {prenom : >25}")
        choix = input("Choix de l'adhérent (q pour sortir): ")
        while (not choix.isdigit() or int(choix) < 1 or int(choix) > len(res)):
            if (choix == 'q'):
                return None
            print("Choix non existant, veuillez choisir un adhérent de la liste.")
            choix = input("Choix de l'adhérent (q pour sortir): ")

        id_adherent_choisi = res[int(choix) - 1][0]

        # on verifie si l'adherent est encore actif (cad il a encore un compte)
        curs.execute("SELECT * FROM compteutilisateur WHERE idadherent = %s;", (id_adherent_choisi,))
        if (curs.fetchone()):
            actif = True
        else:
            actif = False

        return int(id_adherent_choisi), actif

def voir_infos_ressource(curs, codeRessource, type):
    """Pour un code et un type de ressource, affiche les informations disponibles"""
    try:
        if type == "livre":
            curs.execute("""SELECT code, titre, dateapparition, editeur, genre, codeclassification, codelivre, isbn, resume, langue 
                           FROM vLivre WHERE code = %s;""", (codeRessource,))
            res = curs.fetchone()
            if res:
                print("\n=== LIVRE ===")
                print(f"Code: {res[0]}")
                print(f"Titre: {res[1]}")
                print(f"Date d'apparition: {res[2]}")
                print(f"Éditeur: {res[3]}")
                print(f"Genre: {res[4]}")
                print(f"Code classification: {res[5]}")
                print(f"Code livre: {res[6]}")
                print(f"ISBN: {res[7]}")
                print(f"Résumé: {res[8]}")
                print(f"Langue: {res[9]}")
            else:
                print("Livre non trouvé")

        elif type == "film":
            curs.execute("""SELECT code, titre, dateapparition, editeur, genre, codeclassification, codefilm, langue, longueur, synopsis 
                           FROM vFilm WHERE code = %s;""", (codeRessource,))
            res = curs.fetchone()
            if res:
                print("\n=== FILM ===")
                print(f"Code: {res[0]}")
                print(f"Titre: {res[1]}")
                print(f"Date d'apparition: {res[2]}")
                print(f"Éditeur: {res[3]}")
                print(f"Genre: {res[4]}")
                print(f"Code classification: {res[5]}")
                print(f"Code film: {res[6]}")
                print(f"Langue: {res[7]}")
                print(f"Longueur: {res[8]}")
                print(f"Synopsis: {res[9]}")
            else:
                print("Film non trouvé")

        elif type == "music":
            curs.execute("""SELECT code, titre, dateapparition, editeur, genre, codeclassification, codemusique, longueur, style 
                           FROM vOeuvreMusicale WHERE code = %s;""", (codeRessource,))
            res = curs.fetchone()
            if res:
                print("\n=== OEUVRE MUSICALE ===")
                print(f"Code: {res[0]}")
                print(f"Titre: {res[1]}")
                print(f"Date d'apparition: {res[2]}")
                print(f"Éditeur: {res[3]}")
                print(f"Genre: {res[4]}")
                print(f"Code classification: {res[5]}")
                print(f"Code musique: {res[6]}")
                print(f"Longueur: {res[7]}")
                print(f"Style: {res[8]}")
            else:
                print("Oeuvre musicale non trouvée")

    except sql.DatabaseError as err:
        print(f"Erreur de BDD dans voir_infos_ressource : {err}")
        raise

def rechercher_ressource_type(curs):
    print("Recherche")
    print("1 - Un livre")
    print("2 - Un film")
    print("3 - Une Oeuvre musicale")
    choix = input("Choix : ")
    while (choix != '1' and choix != '2' and choix != '3'):
        choix = input("Choix invalide, reéssayez : ")
    if (choix == '1'):
        type = "vLivre"
        contribue = "ContribueLivre"
        code1 = code2 = "codeLivre"
    elif (choix == '2'):
        type = "vFilm"
        contribue = "ContribueFilm"
        code1 = code2 = "codeFilm"
    elif (choix == '3'):
        type = "vOeuvreMusicale"
        contribue = "ContribueMusique"
        code1 = "codeMusique"
        code2 = "codeMusic"
    search = input("Recherche : ")
    mots = search.split()
    param1 = "%"+mots[0]+"%" if len(mots) > 0 else ''
    param2 = "%"+mots[1]+"%" if len(mots) > 1 else ''
    search = "%"+search+"%"
    req = "SELECT t.code, t.titre, t.dateApparition , t.editeur , t.genre, c.nom, c.prenom FROM {type} t LEFT JOIN {contribue} ct ON t.{code1} = ct.{code2} LEFT JOIN Contributeur c ON ct.idContributeur = c.id WHERE LOWER(t.titre) LIKE LOWER(%s) OR LOWER(t.editeur) LIKE LOWER(%s) OR LOWER(t.genre) LIKE LOWER(%s) OR (LOWER(c.prenom) LIKE LOWER(%s) AND LOWER(c.nom) LIKE LOWER(%s));"
    query = req.format(type=type, contribue=contribue, code1=code1, code2=code2)
    try:
        curs.execute(query, (search,search,search,param1,param2))
        res = curs.fetchall()
        if len(res) == 0:
            print("Aucune ressource trouvée.")
            return
        print("\nResultats : ")
        print(f"{'Code' : <10}|{'Titre' : ^40}|{'Auteur' : ^25}|{'Editeur' : ^20}|{'Genre' : ^20}|{'Date' : >15}")
        for result in res:
            print(f"{result[0] : <10}|{result[1] : ^40}|{result[5]+' '+result[6] : ^25}|{result[3] : ^20}|{result[4] : ^20}|{result[2].strftime("%Y-%m-%d") : >15}")
            print("\n")
        print("Tapez le code d'une ressource pour avoir plus d'informations (q pour sortir)")
        choixCode = input("Code : ")
        listPoss = [result[0] for result in res]
        while not choixCode.isdigit() or int(choixCode) not in listPoss :
            if choixCode == 'q':
                return None
            else :
                print("Mauvaise saisie, ressayez")
                choixCode = input("Code : ")
        dictType = {'1': 'livre', '2' : 'film', '3' : 'music'}
        voir_infos_ressource(curs, int(choixCode), dictType[choix])
    except sql.Error as err:
        print("Erreur de BDD : ", err)
        raise


def rechercher_ressource(curs):
    try:
        print("-------------- RECHERCHE D'UNE RESSOURCE -------------- ")
        titre_rech = "%" + input("Titre : ") + "%"
        curs.execute("SELECT code, titre, codeclassification FROM ressource WHERE LOWER(titre) LIKE LOWER(%s);",
                     (titre_rech,))
        res = curs.fetchall()

        if len(res) > 0:
            print(f"{'Code' : <25}|{'Titre' : ^40}|{'Code Classification' : >25}")
            for ressource in res:
                code, titre, codeClass = ressource
                print(f"{code : <25}|{titre : ^40}|{codeClass : >25}")
            codeChoisi = input("Code de la ressource a choisir (q pour sortit): ")
            # verification de l'entrée : bien un id qui est dans la liste proposée
            id_possibles = [item[0] for item in res]
            while (not codeChoisi.isdigit() or int(codeChoisi) not in id_possibles):
                if (codeChoisi == 'q'):
                    return None
                print("Choix non existant, veuillez choisir un exemplaire de la liste.")
                codeChoisi = input("Code de la ressource a choisir (q pour sortit): ")
        else:
            print("Aucune ressource trouvée.")
            codeChoisi = None
        return codeChoisi

    except sql.DatabaseError as err:
        print("Erreur de BDD dans rechercher_ressource : ", err)
        raise


def exemplaire_dispo_from_ressource(curs):
    """propose la liste d'exemplaires dispos et retourne un tuple (codeRessource, idExemplaire)"""
    try:
        codeRessource = rechercher_ressource(curs)
        if (codeRessource is None):
            return None

        # explication de la requetes : tout les exemplaires qui n'ont pas étés empruntée ou qui ont déjà été rendus
        # on utilise un select dans le left join pour n'avoir que les derniers prets et donc ne pas avoir de doublons
        req = """
            SELECT E.id, R.titre, E.etat
                FROM Ressource R
                JOIN Exemplaire E ON R.code = E.code
                WHERE (E.code, E.id) NOT IN
                    (SELECT E.id, E.code FROM pret WHERE pret.daterendu IS NULL)
                AND R.code = %s AND E.etat IN ('neuf', 'bon');
        """
        curs.execute(req, (codeRessource,))
        res = curs.fetchall()
        if (len(res) <= 0):
            print("Aucun exemplaire disponible pour cette ressource.")
            return None

        print(f"{'Id Exemplaire' : <25}|{'Titre Ressource' : ^40}|{'Etat de l\'exemplaire' : >25}")
        for exemplaire in res:
            idE, titre, etat = exemplaire
            print(f"{idE : <25}|{titre : ^40}|{etat : >25}")
        choix = input("Id de l'exemplaire a emprunter (q pour sortit): ")
        # veriication de l'entrée : bien un id qui est dans la liste proposée
        id_possibles = [ex[0] for ex in res]
        while (not choix.isdigit() or int(choix) not in id_possibles):
            if (choix == 'q'):
                # si l'utilisateur souhaite sortir
                return None
            print("Choix non existant, veuillez choisir un exemplaire de la liste.")
            choix = input("Id de l'exemplaire a emprunter : ")

        return codeRessource, choix
    except sql.DatabaseError as err:
        print("Erreur dans exemplaire_dispo_from_ressource : ", err)
        raise


def check_emprunt_permis(curs, idAdherent):
    """verifie si un adherent peut emprunter i.e si il n'a pas de sanction, s'il n'est pas blacklisté et si il a moin de 5 emprunts actifs"""
    # verification si blacklist
    req = """SELECT blackliste FROM adherent WHERE id = %s; """
    curs.execute(req, (idAdherent,))
    res = curs.fetchone()
    if res[0] == True:
        # l'utilisateur est blackliste, il ne peut pas emprunter
        return False

    # verification si sanctions
    req = """SELECT S.typeSanction, S.dateDebut, S.duree
                   FROM Sanction S
                   WHERE S.idAdherent = %s AND (S.duree IS NULL OR (CURRENT_DATE BETWEEN S.dateDebut AND (S.dateDebut + S.duree * INTERVAL '1 day')));
                   """
    # requete : toutes les sanctions dont la date de fin n'est pas dépassée ou qui n'ont pas de date de fin definie (duree = NULL)
    curs.execute(req, (idAdherent,))
    res = curs.fetchone()
    if (res is not None):
        # l'utilisateur a une sanction active, il ne peut pas emprunter
        return False

    # verification si nombre de prets maximal exédé
    req = "SELECT * FROM pret WHERE ida = %s AND daterendu IS NULL;"  # tout les prets non rendus
    curs.execute(req, (idAdherent,))
    res = curs.fetchall()
    if (len(res) >= 5):
        # si l'adhérent a deja emprunté 5 livres il ne peut pas emprunté
        return False

    # sinon on il a le droit
    return True


def inserer_emprunt(curs, conn):
    """Récupère les infos auprès de l'utilisateur et effectue les verifs puis, si tout est bon, l'ajoute a la BDD"""
    try:
        print("-------------- AJOUT D'UN EMPRUNT ------------------ ")

        infosAdherent = rechercher_adherent(curs)
        if (not infosAdherent):
            return None

        idAdherent, actif = infosAdherent

        if (not actif):
            print("Cette adhérent n'est pas actif ! Si il veut emprunter il faut renouveler son abonnement.")
            return None

        # verification si l'adhérent a le droit d'emprunter
        if (not check_emprunt_permis(curs, idAdherent)):
            print(
                "Cet adhérent n'a pas le droit de faire un prêt : sanction, blacklist ou trop d'emprunts actifs (>= 5). ")
            return None

        infosExemplaire = exemplaire_dispo_from_ressource(curs)

        if infosExemplaire is None:
            return None

        codeRessource, idExemplaire = infosExemplaire
        dureePret = input("Duree du pret en nombre de jours : ")
        while (not dureePret.isdigit() or not int(dureePret) > 0):
            dureePret = input("Mauvaise saisie, ressayer : ")

        dureePret = int(dureePret)

        # requete d'insertion
        req = "INSERT INTO pret (code, ide, ida, datepret, duree) VALUES ( %s, %s, %s, CURRENT_DATE, %s);"
        curs.execute(req, (codeRessource, idExemplaire, idAdherent, dureePret))

        # si on arrive ici sans exception, tout s'est bien passé on peu commit
        conn.commit()
        print("Insertion effectuée !")

    except sql.IntegrityError as err:
        conn.rollback()
        print("Les données ne sont pas correctes :  ", err)
        print("Pour information un adhérent ne peut pas emprunter un même exemplaire 2 fois dans la même journée.")

    except sql.DatabaseError as err:
        conn.rollback()
        print("Erreur de BDD l'ors de la creation de l'emprunt: ", err)
        return None


def get_emprunt_adherent(curs, idAdherent):
    """recupere la liste des exemplaires actuellements empruntes par l'adhérent dont l'id est passée en parametre puis
    demande a l'utilisateur d'en choisir une et retourne ce choix
    """
    req = """SELECT exemplaire.id, exemplaire.code, ressource.titre, exemplaire.etat, (pret.datePret + pret.duree * INTERVAL '1 day') AS dateLim  FROM pret 
                JOIN exemplaire ON exemplaire.code = pret.code AND exemplaire.id = pret.idE 
                JOIN ressource ON ressource.code = exemplaire.code 
                WHERE pret.idA = %s AND pret.dateRendu IS NULL;"""
    curs.execute(req, (idAdherent,))
    res = curs.fetchall()
    if (len(res) == 0):
        print("Cette adhérent n'a aucun emprunt.")
        return None

    print(f"{'Choix' : <25}|{'Titre Ressource' : ^40}|{'Etat avant emprunt' : ^20}|{'Date limite' : >25}")
    for ind, exemplaire in enumerate(res):
        idE, codeRessource, titre, etat, dateLim = exemplaire
        print(f"{ind + 1 : <25}|{titre : ^40}|{etat : ^20}|{dateLim.strftime("%Y-%m-%d") : >25}")
    choix = input("Choix de l'exemplaire a emprunter (q pour sortir): ")
    # veriication de l'entrée : bien un id qui est dans la liste proposée
    while (not choix.isdigit() or int(choix) <= 0 or int(choix) > len(res)):
        if (choix == 'q'):
            # si l'utilisateur souhaite sortir
            return None
        print("Choix non existant, veuillez choisir un exemplaire de la liste.")
        choix = input("Id de l'exemplaire a emprunter : ")
    idExemplaire, codeRessource, titreRessource, etat, dateLim = res[int(choix) - 1]
    return idExemplaire, codeRessource, etat, dateLim  # retourne codeRessource, idExemplaire, dateLimite


def get_nb_sanctions(curs, idAdherent):
    """trouve le nombre de sanctions pour un adhérent, utile pour savoir si il faut le blacklister."""
    try:
        req = "SELECT * FROM sanction WHERE idadherent = %s;"
        curs.execute(req, (idAdherent,))
        res = curs.fetchall()
        return len(res)

    except sql.DatabaseError as err:
        print("Erreur de BDD dans get_nb_sanctions : ", err)
        raise


def blacklister(curs, conn, idAdherent):
    try:
        req = "UPDATE adherent SET blackliste = True WHERE id = %s;"
        curs.execute(req, (idAdherent))
    except sql.DatabaseError as err:
        print("Erreur de BDD dans blacklister")
        raise


def sanctionner(curs, conn, idAdherent, type, duree=None):
    """ajoute une sanction a un adhérent, verifie si le nombre de sanctions dépasse le seuil de blacklist, retourne faux en cas de probleme"""
    try:
        req = """INSERT INTO sanction (idadherent, datedebut, duree, typesanction) VALUES (%s,CURRENT_DATE,%s,%s);"""
        curs.execute(req, (idAdherent, duree, type))
        print("Adherent sanctionné.")
        nb_sanctions = get_nb_sanctions(curs, idAdherent)
        # on a arbitrairement fixé a 5 le nombre max de sanctions avant le blacklist
        if (nb_sanctions > 5):
            print("!!! Attention !!! Cette adhérent a deja plus de 5 sanctions.")
            print("Voulez vous le blacklister ? ")
            choix = input("Reponse (oui/non) : ")
            while (choix != "oui" and choix != "non"):
                choix = input("Mauvaise saisie, ressayer. Reponse (oui/non) : ")
            if choix == "oui":
                blacklister(curs, conn, idAdherent)

    except sql.IntegrityError as err:
        print("Erreur dans les données dans sanctionner : ", err)
        raise
    except sql.DatabaseError as err:
        print("Erreur de BDD dans sanctionner : ", err)
        raise


def rendu(curs, conn):
    """Demandes les infos a l'utilisateur pour le rendu d'une ressource et effectue les modifications aux tables,
    propose de sanctionner si besoin."""
    try:
        print("-------------- RENDU D'UN EXEMPLAIRE-------------- ")
        infos_adherent = rechercher_adherent(curs)
        if (infos_adherent is None):
            return None

        idAdherent, actif = infos_adherent

        infos_exemplaire = get_emprunt_adherent(curs, idAdherent)
        if (infos_exemplaire is None):
            return None
        idExemplaire, codeRessource, etat_avant, dateLim = infos_exemplaire

        etats = ["neuf", "bon", "abime", "perdu"]
        print("Quel est l'état actuel de l'exemplaire ?")
        print("1 - neuf\n"
              "2 - bon\n"
              "3 - abime\n"
              "4 - perdu\n")
        choix = input("Votre choix (q pour quitter) :")
        while (not choix.isdigit() or not 0 < int(choix) <= len(etats)):
            if choix == 'q':
                return None
            choix = input("Choix non valide, ressayez : ")
        etat = etats[int(choix) - 1]
        if (etat == 'abime' or etat == 'perdu'):
            print("Cette adhérent a endommagé ou perdu la ressource. Voulez vous le sanctionner ?")
            choix = input("Reponse (oui/non): ")
            while choix not in ["oui", "non"]:
                choix = input("Choix non valide, ressayez : ")
            if choix == "oui":
                type = "perte" if (etat == "perdu") else "degradation"
                sanctionner(curs, conn, idAdherent, type)
        if (datetime.today() > dateLim):
            # Tout retard dans la restitution des documents empruntés entraîne une suspension du
            # droit de prêt d'une durée égale au nombre de jours de retard
            print("Ce retour est en retard. Voulez vous sanctionner l'adhérent ?")
            choix = input("Reponse (oui/non) : ")
            while choix not in ["oui", "non"]:
                choix = input("Choix non valide, ressayez : ")
            if (choix == "oui"):
                diff = datetime.today() - dateLim
                sanctionner(curs, conn, idAdherent, 'retard', diff.days)

        # on note que l'emprunt est rendu
        req = """UPDATE pret SET daterendu = CURRENT_DATE WHERE ida = %s AND code = %s AND ide = %s;"""
        curs.execute(req, (idAdherent, codeRessource, idExemplaire))

        # on note le nouvel etat de la ressource si il a changé
        if (etat != etat_avant):
            req = """UPDATE exemplaire SET etat = %s WHERE id = %s AND code = %s;"""
            curs.execute(req, (etat, idExemplaire, codeRessource))

        print("Le rendu a été effectué !")
        conn.commit()

    except sql.IntegrityError as err:
        print("Erreur avec les données : ", err)
        if conn:
            conn.rollback()
        return None
    except sql.DatabaseError as err:
        print("Erreur de BDD : ", err)
        if conn:
            conn.rollback()
        return None
def rembourser_ressource(curs, conn):
    try:
        print("-------------- REMBOURSER UNE RESSOURCE--------------")
        infos_adherent = rechercher_adherent(curs)
        if (infos_adherent is None):
            return None
        idAdherent = infos_adherent[0]
        # on recupère toute les sanctions qui sont en rapport avec une perte ou une dégradation (durée non determinée)
        req = """SELECT id,dateDebut, typesanction FROM sanction WHERE idadherent = %s AND duree IS NULL;"""
        curs.execute(req, (idAdherent,))
        res = curs.fetchall()
        if (len(res) == 0):
            print("Cette adhérent n'a aucune sanction lié a une perte ou une dégradation.")
        else:
            print(f"{'id sanction' : <25}|{'Date début' : ^40}|{'Type sanction' : >25}")
            for sanction in res:
                idSanction,dateDebut, typeSanction = sanction
                print(f"{idSanction : <25}|{dateDebut.strftime("%Y-%m-%d")  : ^40}|{typeSanction  : >25}")
            choixId = input("Id de la sanction concernée (q pour sortir) : ")
            choixPossibles = [sanction[0] for sanction in res]
            while (not choixId.isdigit() or int(choixId) not in choixPossibles):
                if (choixId == "q"):
                    return None
                choixId = input("Mauvaise saisie, ressayer, Id de la sanction concernée : ")
            req = """UPDATE sanction SET duree = (CURRENT_DATE - dateDebut - 1) WHERE idadherent = %s AND id = %s;"""
            curs.execute(req, (idAdherent, choixId))
            conn.commit()
            print("La sanction a été terminée.")
    except sql.DatabaseError as err:
        print("Erreur de BDD dans rembourser_docuement : ", err)
        conn.rollback()
        return None

def desinscrire_adherent(curs, conn):
    try:
        info_adherent = rechercher_adherent(curs)
        if (info_adherent is None):
            return None
        idAdherent, actif = info_adherent
        if actif:
            req = "DELETE FROM compteutilisateur WHERE idadherent = %s;"
            curs.execute(req, (idAdherent,))
            conn.commit()
            print("L'adhérent a été desinscrit.")
        else:
            print("L'adhérent n'était déjà pas inscrit. ")
    except sql.DatabaseError as err:
        print("Erreur de BDD dans desinscrire_adherent : ", err)
        return None



def afficher_informations_adherent(curs, idAdherent):
    """Affiche toutes les informations concernant un adhérent spécifique et liste ses prêts."""
    req_info = """
        SELECT id, nom, prenom, email, blackliste
        FROM adherent
        WHERE id = %s;
    """
    req_prets = """
        SELECT P.code, R.titre, P.datePret, P.duree, P.dateRendu
        FROM pret P
        JOIN ressource R ON P.code = R.code
        WHERE P.idA = %s
        ORDER BY P.datePret DESC;
    """
    try:
        # Affichage des informations de l'adhérent
        curs.execute(req_info, (idAdherent,))
        res = curs.fetchone()
        if res:
            print(f"ID: {res[0]}, Nom: {res[1]}, Prénom: {res[2]}, Email: {res[3]}, Blacklisté: {'Oui' if res[4] else 'Non'}")
        else:
            print("Aucun adhérent trouvé avec cet ID.")
            return

        # Affichage des prêts de l'adhérent
        curs.execute(req_prets, (idAdherent,))
        prets = curs.fetchall()
        if prets:
            print("\nListe des prêts:")
            print(f"{'Code' : <10} | {'Titre' : <30} | {'Date de prêt' : <20} | {'Durée (jours)' : <15} | {'Date de rendu' : <20}")
            for pret in prets:
                date_rendu = pret[4] if pret[4] else "En cours"
                print(f"{pret[0] : <10} | {pret[1] : <30} | {pret[2].strftime('%Y-%m-%d') : <20} | {pret[3] : <15} | {date_rendu}")
        else:
            print("Aucun prêt trouvé pour cet adhérent.")
    except sql.Error as err:
        print("Erreur lors de la récupération des informations de l'adhérent: ", err)

def afficher_statistiques_adherent(curs, idAdherent):
    """Affche les informations concernant l'adhérent. """

    afficher_informations_adherent(curs, idAdherent)

    """Affiche les statistiques"""
    # Statistiques sur les prêts
    req_prets = "SELECT COUNT(*) FROM pret WHERE idA = %s;"
    curs.execute(req_prets, (idAdherent,))
    nb_prets = curs.fetchone()[0]

    # Statistiques sur les retards
    req_retards = "SELECT COUNT(*) FROM pret WHERE idA = %s AND dateRendu > (datePret + duree * INTERVAL '1 day');"
    curs.execute(req_retards, (idAdherent,))
    nb_retards = curs.fetchone()[0]

    # Statistiques sur les sanctions
    req_sanctions = "SELECT COUNT(*) FROM sanction WHERE idAdherent = %s;"
    curs.execute(req_sanctions, (idAdherent,))
    nb_sanctions = curs.fetchone()[0]

    # Affichage des statistiques
    print(f"Statistiques pour l'adhérent ID {idAdherent}:")
    print(f"Nombre total de prêts: {nb_prets}")
    print(f"Nombre de prêts en retard: {nb_retards}")
    print(f"Nombre de sanctions: {nb_sanctions}")

def voir_statistiques_adherent(curs):
    """Permet au personnel de voir les statistiques d'un adhérent après l'avoir recherché."""
    print("Recherche de l'adhérent pour voir ses statistiques:")
    infosAdherent = rechercher_adherent(curs)
    if infosAdherent:
        idAdherent, actif = infosAdherent
        afficher_statistiques_adherent(curs, idAdherent)
    else:
        print("Aucun adhérent sélectionné ou trouvé.")

def ajouter_ressource(curs):
    try:
        # Demander les informations de base pour la ressource
        code = int(input("Entrez le code de la ressource : "))
        titre = input("Entrez le titre de la ressource : ")
        date_apparition = input("Entrez la date d'apparition (YYYY-MM-DD) : ")
        editeur = input("Entrez l'éditeur : ")
        genre = input("Entrez le genre : ")
        code_classification = input("Entrez le code de classification : ")

        # Insérer la ressource de base
        curs.execute(
            """
            INSERT INTO Ressource (code, titre, dateApparition, editeur, genre, codeClassification)
            VALUES (%s, %s, %s, %s, %s, %s)
            """,
            (code, titre, date_apparition, editeur, genre, code_classification)
        )

        # Demander le type spécifique de la ressource
        type_ressource = input("Est-ce un Livre, un Film ou une Oeuvre musicale ? (l/f/m) : ").lower()

        if type_ressource == 'l':
            # Ajouter un livre
            isbn = input("Entrez l'ISBN : ")
            resume = input("Entrez le résumé : ")
            langue = input("Entrez la langue : ")
            curs.execute(
                """
                INSERT INTO Livre (codeLivre, ISBN, resume, langue)
                VALUES (%s, %s, %s, %s)
                """,
                (code, isbn, resume, langue)
            )

        elif type_ressource == 'f':
            # Ajouter un film
            langue = input("Entrez la langue : ")
            longueur = int(input("Entrez la longueur (en minutes) : "))
            synopsis = input("Entrez le synopsis : ")
            curs.execute(
                """
                INSERT INTO Film (codeFilm, langue, longueur, synopsis)
                VALUES (%s, %s, %s, %s)
                """,
                (code, langue, longueur, synopsis)
            )

        elif type_ressource == 'm':
            # Ajouter une oeuvre musicale
            longueur = int(input("Entrez la longueur (en secondes) : "))
            style = input("Entrez le style musical : ")
            curs.execute(
                """
                INSERT INTO OeuvreMusicale (codeMusique, longueur, style)
                VALUES (%s, %s, %s)
                """,
                (code, longueur, style)
            )

        else:
            print("Type de ressource invalide. Aucune donnée spécifique n'a été ajoutée.")

        print("Ressource ajoutée avec succès !")

    except Exception as e:
        print(f"Erreur lors de l'ajout de la ressource : {e}")
        raise

def modifier_description(curs):
    try:
        # Recherche de la ressource
        code = rechercher_ressource(curs)
        if not code:
            return

        # Vérifier si la ressource est un livre ou un film
        curs.execute(
            """
            SELECT 'livre' AS type FROM Livre WHERE codeLivre = %s
            UNION ALL
            SELECT 'film' AS type FROM Film WHERE codeFilm = %s
            """,
            (code, code)
        )
        resultat = curs.fetchone()

        if not resultat:
            print("La ressource sélectionnée n'est ni un livre ni un film.")
            return

        type_ressource = resultat[0]

        # Modifier la description en fonction du type de ressource
        if type_ressource == 'livre':
            nouveau_resume = input("Entrez le nouveau résumé du livre : ")
            curs.execute(
                """
                UPDATE Livre SET resume = %s WHERE codeLivre = %s
                """,
                (nouveau_resume, code)
            )
        elif type_ressource == 'film':
            nouveau_synopsis = input("Entrez le nouveau synopsis du film : ")
            curs.execute(
                """
                UPDATE Film SET synopsis = %s WHERE codeFilm = %s
                """,
                (nouveau_synopsis, code)
            )

        print("Description mise à jour avec succès !")

    except Exception as e:
        print(f"Erreur lors de la modification de la description : {e}")
        raise

def statistiques(curs):
    choix_stat = ''
    while choix_stat != '0':
        print("\n--------------------- STATISTIQUES ---------------------")
        print("0 - Retour au menu principal")
        print("1 - Nombre total d’adhérents et de membres du personnel")
        print("2 - Documents les plus empruntés (limité à 10)")
        print("3 - Types de ressources les plus populaires")
        print("4 - Documents les plus empruntés par genre")
        print("5 - Documents ayant la plus haute moyenne de durée d’emprunts")
        print("6 - Préférence des adhérents par type de document")
        choix_stat = input("Choisissez une statistique : ")

        match choix_stat:
            case '0':
                continue
            case '1':
                req = """
                SELECT 
                    (SELECT COUNT(*) FROM Adherent) AS nbAdherents, 
                    (SELECT COUNT(*) FROM MembrePersonnel) AS nbPersonnel;
                """
                curs.execute(req)
                res = curs.fetchone()
                print(f"Nombre total d'adhérents : {res[0]}, Nombre total de membres du personnel : {res[1]}")
            case '2':
                req = """
                SELECT R.titre, COUNT(P.code) AS nombre_emprunts
                FROM Ressource R
                JOIN Exemplaire E ON R.code = E.code
                JOIN Pret P ON E.code = P.code AND E.id = P.idE
                GROUP BY R.titre
                ORDER BY nombre_emprunts DESC
                LIMIT 10;
                """
                curs.execute(req)
                res = curs.fetchall()
                print(f"{'Titre':<40} | {'Nombre d’emprunts':>15}")
                for row in res:
                    print(f"{row[0]:<40} | {row[1]:>15}")
            case '3':
                req = """
                SELECT CASE
                    WHEN R.code IN (SELECT codeLivre FROM Livre) THEN 'Livre'
                    WHEN R.code IN (SELECT codeFilm FROM Film) THEN 'Film'
                    WHEN R.code IN (SELECT codeMusique FROM OeuvreMusicale) THEN 'Musique'
                END AS type_document,
                COUNT(P.code) AS nombre_emprunts
                FROM Ressource R
                JOIN Exemplaire E ON R.code = E.code
                JOIN Pret P ON E.code = P.code AND E.id = P.idE
                GROUP BY type_document
                ORDER BY nombre_emprunts DESC;
                """
                curs.execute(req)
                res = curs.fetchall()
                print(f"{'Type de document':<15} | {'Nombre d’emprunts':>15}")
                for row in res:
                    print(f"{row[0]:<15} | {row[1]:>15}")
            case '4':
                print("Choisissez un genre : ")
                print("1 - Livres\n2 - Films\n3 - Œuvres musicales")
                genre_choix = input("Votre choix : ")
                genre_req = {
                    '1': ("vLivre", "titre"),
                    '2': ("vFilm", "titre"),
                    '3': ("vOeuvreMusicale", "titre"),
                }
                if genre_choix in genre_req:
                    req = f"""
                    SELECT {genre_req[genre_choix][1]}, COUNT(P.code) AS nombre_emprunts
                    FROM {genre_req[genre_choix][0]} AS V
                    JOIN Exemplaire E ON V.code = E.code
                    JOIN Pret P ON E.code = P.code AND E.id = P.idE
                    GROUP BY {genre_req[genre_choix][1]}
                    ORDER BY nombre_emprunts DESC
                    LIMIT 10;
                    """
                    curs.execute(req)
                    res = curs.fetchall()
                    print(f"{'Titre':<40} | {'Nombre d’emprunts':>15}")
                    for row in res:
                        print(f"{row[0]:<40} | {row[1]:>15}")
                else:
                    print("Choix invalide.")
            case '5':
                req = """
                SELECT R.titre, AVG(P.duree) AS duree_moyenne
                FROM Ressource R
                JOIN Exemplaire E ON R.code = E.code
                JOIN Pret P ON E.code = P.code AND E.id = P.idE
                GROUP BY R.titre
                ORDER BY duree_moyenne DESC
                LIMIT 10;
                """
                curs.execute(req)
                res = curs.fetchall()
                print(f"{'Titre':<40} | {'Durée moyenne (jours)':>25}")
                for row in res:
                    print(f"{row[0]:<40} | {row[1]:>25.2f}")
            case '6':
                req = """
                SELECT A.nom, A.prenom, 
                CASE
                    WHEN R.code IN (SELECT codeLivre FROM Livre) THEN 'Livre'
                    WHEN R.code IN (SELECT codeFilm FROM Film) THEN 'Film'
                    WHEN R.code IN (SELECT codeMusique FROM OeuvreMusicale) THEN 'Musique'
                END AS type_document,
                COUNT(P.code) AS nombre_emprunts
                FROM Adherent A
                JOIN Pret P ON A.id = P.idA
                JOIN Exemplaire E ON P.code = E.code AND P.idE = E.id
                JOIN Ressource R ON E.code = R.code
                GROUP BY A.nom, A.prenom, type_document
                ORDER BY nombre_emprunts DESC;
                """
                curs.execute(req)
                res = curs.fetchall()
                print(f"{'Nom et prénom':<30} | {'Type de document':^20} | {'Nombre d’emprunts':>15}")
                for row in res:
                    print(f"{row[0] + row[1]: <30} | {row[2]:^20} | {row[3]:>15}")
            case _:
                print("Choix invalide, veuillez réessayer.")

def programme_adherent(curs, id_adherent):
    choix = ''
    while (choix != '0'):
        print("--------------------- Interface Adhérent ---------------------")
        print("0 - Quitter")
        print("1 - Voir toutes vos informations")
        print("2 - Afficher toutes les ressources")
        print("3 - Chercher une ressource")
        choix = input("Choix : ")

        match choix:
            case '0':
                print("Programme fini. ")
                continue
            case '1':
                afficher_informations_adherent(curs, id_adherent)
            case '2':
                afficher_ressources(curs)
                continue
            case '3':
                rechercher_ressource_type(curs)
                continue
            case  _ :
                print("Mauvaise entrée, reéssayez.")


def programme_membre_perso(curs, conn):
    choix = ''
    while (choix != '0'):
        print("--------------------- Interface Personnel ---------------------")
        print("0 - Quitter")
        print("1 - Ajouter un pret")
        print("2 - Rendu d'une ressource")
        print("3 - Desinscrire un adherent")
        print("4 - Ajouter un adhérent")
        print("5 - Voir les statistiques d'un adhérent")
        print("6 - Ajouter une ressource")
        print("7 - Modifier la description d'une ressource")
        print("8 - Voir les statistiques générales")
        print("9 - Rembourser une ressource")

        choix = input("Choix : ")

        match choix:
            case '0':
                print("Programme fini. ")
                continue
            case '1':
                inserer_emprunt(curs, conn)
                continue
            case '2':
                rendu(curs, conn)
                continue
            case '3':
                desinscrire_adherent(curs, conn)
                continue
            case '4':
                ajouter_nouvelle_adherent(curs, conn)
                continue
            case '5':
                voir_statistiques_adherent(curs)
                continue
            case '6':
                ajouter_ressource(curs)
                continue
            case '7':
                modifier_description(curs)
                continue
            case '8':
                statistiques(curs)
                continue
            case '9':
                rembourser_ressource(curs, conn)
                continue
            case _:
                print("Mauvaise entrée, reéssayez.")


def main():
    # test adherent : username = sophiemartin, password = sophiePass
    # test membre perso : username = camilleroux, password = admin123
    try:
        conn = sql.connect(host=HOST, database=DB, user=USER, password=PW)
        curs = conn.cursor()
        atexit.register(cleanup, curs, conn)  # pour bien tout fermer en cas d'interruption
        print("Connexion :")
        res = login(curs)
        essais = 0
        while (not res and essais < 3):
            essais += 1
            print("Mauvais utilisateur ou mot de passe, ressayer")
            res = login(curs)
        if res:
            id_adherent, id_membre = res
            if (id_membre and not id_adherent):
                programme_membre_perso(curs, conn)
            elif (id_adherent and not id_membre):
                programme_adherent(curs, id_adherent)
        else:  # si res est None c'est que après le max d'essais on a toujours pas réussi
            print("Nombre d'essais maximale atteint, abandon du login.")


    except sql.DatabaseError as err:
        print("Erreur de BDD dans main: ", err)


def cleanup(curs, conn):
    if curs:
        curs.close()
    if conn:
        conn.close()


if __name__ == '__main__':
    main()
