﻿-- Le contributeur avec l'ID 2 (Christopher Nolan) n'est pas marqué comme auteur, donc cela ne devrait pas fonctionner


INSERT INTO ContribueLivre (idContributeur, codeLivre)
VALUES (2, 3);


-- Cette insertion échouera car le trigger vérifie que le contributeur doit être auteur.




-- Le contributeur avec l'ID 4 (Jules Verne) n'est ni compositeur ni interprète


INSERT INTO ContribueMusique (idContributeur, codeMusic)
VALUES (4, 1);


-- Cette insertion échouera car le trigger  vérifie que le contributeur doit être compositeur ou interprète.