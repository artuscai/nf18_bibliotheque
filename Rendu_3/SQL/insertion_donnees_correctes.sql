-- Insertion de données fictives dans la table Contributeur
INSERT INTO Contributeur (id, nom, prenom, dateNaissance, nationalite, compositeur, interprete, auteur, realisateur, acteur)
VALUES
(1, 'Beethoven', 'Ludwig', '1770-12-17', 'Allemand', TRUE, FALSE, FALSE, FALSE, FALSE),
(2, 'Nolan', 'Christopher', '1970-07-30', 'Américain', FALSE, FALSE, FALSE, TRUE, FALSE),
(3, 'Verne', 'Jules', '1828-02-08', 'Français', FALSE, FALSE, TRUE, FALSE, FALSE),
(4, 'Mozart', 'Wolfgang', '1756-01-27', 'Autrichien', TRUE, TRUE, FALSE, FALSE, FALSE),
(5, 'Spielberg', 'Steven', '1946-12-18', 'Américain', FALSE, FALSE, FALSE, TRUE, FALSE),
(6, 'Piaf', 'Édith', '1915-12-19', 'Française', FALSE, TRUE, FALSE, FALSE, FALSE),
(7, 'Chaplin', 'Charlie', '1889-04-16', 'Britannique', FALSE, FALSE, FALSE, TRUE, TRUE),
(8, 'Rowling', 'J.K.', '1965-07-31', 'Britannique', FALSE, FALSE, TRUE, FALSE, FALSE),
(9, 'Gaga', 'Lady', '1986-03-28', 'Américaine', TRUE, TRUE, FALSE, FALSE, TRUE),
(10, 'Tarantino', 'Quentin', '1963-03-27', 'Américain', FALSE, FALSE, FALSE, TRUE, TRUE);


-- Insertion de données fictives dans la table Ressource
INSERT INTO Ressource (code, titre, dateApparition, editeur, genre, codeClassification)
VALUES
(1, 'Symphonie No. 5', '1808-12-22', 'Inconnu', 'Classique', 'MUS123'),
(2, 'Inception', '2010-07-16', 'Warner Bros.', 'Science-fiction', 'FILM456'),
(3, 'Vingt mille lieues sous les mers', '1870-01-01', 'Pierre-Jules Hetzel', 'Aventure', 'BOOK789'),
(4, 'Requiem', '1791-12-05', 'Nannerl Publishing', 'Classique', 'MUS321'),
(5, 'La Liste de Schindler', '1993-11-30', 'Universal Pictures', 'Drame', 'FILM654'),
(6, 'La Vie en Rose', '1956-02-15', 'Columbia Records', 'Chanson', 'MUS741'),
(7, 'Les Temps Modernes', '1936-02-25', 'United Artists', 'Comédie', 'FILM852'),
(8, 'Harry Potter à l’école des sorciers', '1997-06-26', 'Bloomsbury', 'Fantaisie', 'BOOK963');


-- Insertion de données fictives dans la table Livre
-- Ajout de livres avec des codes et des ISBN correspondants
INSERT INTO Livre (codeLivre, ISBN, resume, langue)
VALUES
(3, '978-0140624378', 'Un roman de science-fiction classique sur le sous-marin Nautilus.', 'Français'),
(8, '978-0747532699', 'Le premier livre de la série Harry Potter, introduisant le jeune sorcier.', 'Anglais');


-- Insertion de données fictives dans la table OeuvreMusicale
-- Ajout de compositions musicales avec des durées et des styles
INSERT INTO OeuvreMusicale (codeMusique, longueur, style)
VALUES
(1, 450, 'Symphonie'),
(4, 3600, 'Requiem'),
(6, 210, 'Chanson');


-- Insertion de données fictives dans la table Film
-- Ajout de films avec leur langue, durée et synopsis
INSERT INTO Film (codeFilm, langue, longueur, synopsis)
VALUES
(2, 'Anglais', 148, 'Un voleur qui utilise la technologie de partage de rêves pour voler des secrets d’entreprise.'),
(5, 'Anglais', 195, 'Un film sur les efforts humanitaires d’Oskar Schindler pendant la Seconde Guerre mondiale.'),
(7, 'Anglais', 87, 'Une comédie qui satirise l’ère industrielle.');


-- Insertion de données fictives dans la table ContribueLivre
-- Attribution des contributions d'auteurs à des livres
INSERT INTO ContribueLivre (idContributeur, codeLivre)
VALUES
(3, 3), -- Jules Verne contribue au livre "Vingt mille lieues sous les mers".
(8, 8); -- J.K. Rowling contribue au livre "Harry Potter à l’école des sorciers".


-- Insertion de données fictives dans la table ContribueFilm
-- Attribution des contributions de réalisateurs et d'acteurs aux films
INSERT INTO ContribueFilm (idContributeur, codeFilm)
VALUES
(2, 2), -- Christopher Nolan contribue au film "Inception" en tant que réalisateur.
(5, 5), -- Steven Spielberg contribue au film "La Liste de Schindler".
(7, 7), -- Charlie Chaplin contribue au film "Les Temps Modernes".
(10, 5); -- Quentin Tarantino contribue au film "La Liste de Schindler".


-- Insertion de données fictives dans la table ContribueMusique
-- Attribution des contributions de compositeurs et interprètes aux œuvres musicales
INSERT INTO ContribueMusique (idContributeur, codeMusic)
VALUES
(1, 1), -- Beethoven contribue à la "Symphonie No. 5".
(4, 4), -- Mozart contribue au "Requiem".
(6, 6), -- Édith Piaf contribue à "La Vie en Rose".
(9, 6); -- Lady Gaga contribue également en tant qu'interprète.




-- Insertion de données fictives dans la table Exemplaire
-- Ajout d'exemplaires de ressources avec l'état précisé
INSERT INTO Exemplaire (code, id, etat)
VALUES
(1, 1, 'neuf'), -- Exemplaire de la Symphonie No. 5
(1, 2, 'bon'), -- Deuxième exemplaire de la Symphonie No. 5
(3, 1, 'neuf'), -- Exemplaire du livre "Vingt mille lieues sous les mers"
(8, 1, 'bon'), -- Exemplaire du livre "Harry Potter à l’école des sorciers"
(2, 1, 'neuf'), -- Exemplaire du film "Inception"
(5, 1, 'bon'), -- Exemplaire du film "La Liste de Schindler"
(7, 1, 'abime'), -- Exemplaire du film "Les Temps Modernes"
(6, 1, 'neuf'); -- Exemplaire de la chanson "La Vie en Rose"


-- Insertion de données fictives dans la table Adherent
-- Ajout d'adhérents avec des détails personnels
INSERT INTO Adherent (id, nom, prenom, email, blackliste)
VALUES
(1, 'Dupont', 'Jean', 'jean.dupont@example.com', FALSE),
(2, 'Martin', 'Sophie', 'sophie.martin@example.com', FALSE),
(3, 'Bernard', 'Luc', 'luc.bernard@example.com', TRUE), -- Adhérent blacklisté
(4, 'Durand', 'Marie', 'marie.durand@example.com', FALSE),
(5, 'Lemoine', 'Pierre', 'pierre.lemoine@example.com', FALSE);


-- Insertion de données fictives dans la table MembrePersonnel
-- Ajout de membres du personnel de la bibliothèque
INSERT INTO MembrePersonnel (id, nom, prenom, adresse, email)
VALUES
(1, 'Roux', 'Camille', '10 rue des Lilas, Paris', 'camille.roux@example.com'),
(2, 'Leroy', 'Thomas', '15 avenue des Champs, Lyon', 'thomas.leroy@example.com');


-- Insertion de données fictives dans la table CompteUtilisateur
-- Création de comptes utilisateurs pour les adhérents et le personnel
INSERT INTO CompteUtilisateur (login, motDePasse, idAdherent, idPersonnel)
VALUES
('jeandupont', 'password123', 1, NULL), -- Compte pour Jean Dupont (adhérent)
('sophiemartin', 'sophiePass', 2, NULL), -- Compte pour Sophie Martin (adhérent)
('lucbernard', 'bernard123', 3, NULL), -- Compte pour Luc Bernard (adhérent blacklisté)
('camilleroux', 'admin123', NULL, 1), -- Compte pour Camille Roux (personnel)
('thomasleroy', 'staff123', NULL, 2); -- Compte pour Thomas Leroy (personnel)


-- Insertion de données fictives dans la table Sanction
-- Ajout de sanctions pour les adhérents
INSERT INTO Sanction (idAdherent, id, dateDebut, duree, typeSanction)
VALUES
(3, 1, '2023-05-01', 30, 'retard'), -- Sanction pour Luc Bernard (retard)
(1, 2, '2023-06-15', 15, 'degradation'); -- Sanction pour Jean Dupont (dégradation)


-- Insertion de données fictives dans la table Pret
-- Ajout d'enregistrements de prêts de ressources
INSERT INTO Pret (code, idE, idA, datePret, duree, dateRendu)
VALUES
(1, 1, 1, '2023-09-01', 14, '2023-09-15'), -- Jean Dupont emprunte un exemplaire de la Symphonie No. 5 et le rend
(3, 1, 2, '2024-09-05', 21, NULL), -- Sophie Martin emprunte "Vingt mille lieues sous les mer" et ne l'a toujours pas rendu
(2, 1, 4, '2023-09-10', 7, NULL), -- Marie Durand emprunte "Inception et ne l'a toujours pas rendu"
(8, 1, 5, '2023-09-15', 30, '2024-01-01'); -- Pierre Lemoine emprunte "Harry Potter à l’école des sorciers et le rend en retard"