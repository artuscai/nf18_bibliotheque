DROP VIEW  IF EXISTS vCompositeur;
DROP VIEW  IF EXISTS vInterprete;
DROP VIEW  IF EXISTS vAuteur;
DROP VIEW  IF EXISTS vActeur;
DROP VIEW  IF EXISTS vRealisteur;
DROP VIEW  IF EXISTS vOeuvreMusicale ;
DROP VIEW  IF EXISTS vLivre ;
DROP VIEW  IF EXISTS vFilm;
DROP TABLE IF EXISTS Pret;
DROP TABLE IF EXISTS Exemplaire;
DROP TABLE IF EXISTS ContribueLivre;
DROP TABLE IF EXISTS ContribueFilm;
DROP TABLE IF EXISTS ContribueMusique;
DROP TABLE IF EXISTS Contributeur;
DROP TABLE IF EXISTS Livre;
DROP TABLE IF EXISTS OeuvreMusicale;
DROP TABLE IF EXISTS Film;
DROP TABLE IF EXISTS Ressource;
DROP TABLE IF EXISTS Sanction;
DROP TABLE IF EXISTS CompteUtilisateur;
DROP TABLE IF EXISTS MembrePersonnel;
DROP TABLE IF EXISTS Adherent;




CREATE TABLE MembrePersonnel (
        id INTEGER PRIMARY KEY,
        nom VARCHAR NOT NULL,
        prenom VARCHAR NOT NULL,
        adresse VARCHAR NOT NULL,
        email VARCHAR
);
    
    


CREATE TABLE Adherent(
        id INTEGER PRIMARY KEY,
        nom VARCHAR(50) NOT NULL,
        prenom VARCHAR(50) NOT NULL,
        email VARCHAR(100),
        blackliste BOOLEAN
);






CREATE TABLE CompteUtilisateur (
login VARCHAR(100) PRIMARY KEY,
motDePasse VARCHAR(100) NOT NULL,
idAdherent INT,
idPersonnel INT,
FOREIGN KEY (idAdherent) REFERENCES Adherent(id),
FOREIGN KEY (idPersonnel) REFERENCES MembrePersonnel(id),
CHECK ( (idAdherent IS NOT NULL AND idPersonnel IS NULL) OR (idAdherent IS NULL AND idPersonnel IS NOT NULL) )
);




CREATE TABLE Sanction (
idAdherent INT,
id INT,
dateDebut DATE,
duree INT,
typeSanction VARCHAR(12),
PRIMARY KEY (idAdherent, id),
FOREIGN KEY (idAdherent) REFERENCES Adherent(id),
CHECK ( typeSanction = 'retard' OR typeSanction = 'degradation' OR typeSanction = 'perte')
);




CREATE TABLE Ressource (
code INT PRIMARY KEY,
titre VARCHAR(255),
dateApparition DATE,
editeur VARCHAR(100),
genre VARCHAR(50),
codeClassification VARCHAR(50)
);


CREATE TABLE Livre (
codeLivre INT PRIMARY KEY,
ISBN VARCHAR(20) UNIQUE,
resume TEXT,
langue VARCHAR(50),
FOREIGN KEY (codeLivre) REFERENCES Ressource(code)
);


CREATE TABLE OeuvreMusicale (
        codeMusique INTEGER PRIMARY KEY,
        longueur INTEGER,
        style VARCHAR,
        FOREIGN KEY (codeMusique) REFERENCES Ressource(code)
);




CREATE TABLE Film (
codeFilm INT PRIMARY KEY,
langue VARCHAR(50),
longueur INT,
synopsis TEXT,
FOREIGN KEY (codeFilm) REFERENCES Ressource(code)
);


CREATE TABLE Contributeur (
        id INTEGER PRIMARY KEY,
        nom VARCHAR NOT NULL,
        prenom VARCHAR NOT NULL,
        dateNaissance DATE,
        nationalite VARCHAR,
        compositeur BOOLEAN,
        interprete BOOLEAN,
        auteur BOOLEAN,
        realisateur BOOLEAN,
        acteur BOOLEAN,
        CHECK (compositeur OR interprete OR auteur OR realisateur OR acteur)
);
    
CREATE TABLE ContribueLivre (
        idContributeur INTEGER,
        codeLivre INTEGER,
        FOREIGN KEY (idContributeur) REFERENCES Contributeur(id),
        FOREIGN KEY (codeLivre) REFERENCES Livre(codeLivre),
        PRIMARY KEY (idContributeur, codeLivre)
);


CREATE OR REPLACE FUNCTION check_auteur()
RETURNS TRIGGER AS $$
BEGIN
IF NOT EXISTS (
SELECT 1
FROM Contributeur
WHERE id = NEW.idContributeur AND auteur = TRUE)
THEN
RAISE EXCEPTION 'Le contributeur doit être auteur pour contribuer à un livre.';
END IF;
RETURN NEW;
END;
$$ LANGUAGE plpgsql;




CREATE TRIGGER check_auteur_trigger
BEFORE INSERT ON ContribueLivre
FOR EACH ROW
EXECUTE FUNCTION check_auteur();


CREATE TABLE ContribueFilm (
        idContributeur INTEGER,
        codeFilm INTEGER,
        FOREIGN KEY (idContributeur) REFERENCES Contributeur(id),
        FOREIGN KEY (codeFilm) REFERENCES Film(codeFilm),
        PRIMARY KEY (idContributeur, codeFilm)
);


CREATE OR REPLACE FUNCTION check_realisateur_or_acteur()
RETURNS TRIGGER AS $$
BEGIN
IF NOT EXISTS (
SELECT 1
FROM Contributeur
WHERE id = NEW.idContributeur AND (realisateur = TRUE OR acteur = TRUE) )
THEN
RAISE EXCEPTION 'Le contributeur doit être réalisateur ou acteur pour contribuer à un film.';
END IF;
RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER check_realisateur_or_acteur_trigger
BEFORE INSERT ON ContribueFilm
FOR EACH ROW
EXECUTE FUNCTION check_realisateur_or_acteur();




CREATE TABLE ContribueMusique (
        idContributeur INTEGER,
        codeMusic INTEGER,
        FOREIGN KEY (idContributeur) REFERENCES Contributeur(id),
        FOREIGN KEY (codeMusic) REFERENCES OeuvreMusicale(codeMusique),
        PRIMARY KEY (idContributeur, codeMusic)
);


CREATE OR REPLACE FUNCTION check_compositeur_or_interprete()
RETURNS TRIGGER AS $$
BEGIN
IF NOT EXISTS (
SELECT 1
FROM Contributeur
WHERE id = NEW.idContributeur AND (compositeur = TRUE OR interprete= TRUE) )
THEN
RAISE EXCEPTION 'Le contributeur doit être compositeur ou interprete pour contribuer à une oeuvre musicale.';
END IF;
RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER check_compositeur_or_interprete_trigger
BEFORE INSERT ON ContribueMusique
FOR EACH ROW
EXECUTE FUNCTION check_compositeur_or_interprete();




CREATE TABLE Exemplaire (
code INT,
id  SERIAL,
etat VARCHAR(10),
PRIMARY KEY (code, id),
FOREIGN KEY (code) REFERENCES Ressource(code),
CHECK ( etat = 'neuf' OR etat = 'bon' OR etat = 'abime' OR etat = 'perdu')
);


CREATE TABLE Pret (
        code INTEGER,
        idE INTEGER,
        idA INTEGER,
        datePret DATE NOT NULL,
        duree INTEGER NOT NULL,
        dateRendu DATE,
        FOREIGN KEY (code, idE) REFERENCES Exemplaire(code, id),
        FOREIGN KEY (idA) REFERENCES Adherent(id),
        PRIMARY KEY (code, idE, idA, datePret)
);
    


CREATE VIEW vCompositeur AS
        SELECT * FROM Contributeur WHERE compositeur = True ;


CREATE VIEW vInterprete AS
        SELECT * FROM Contributeur WHERE interprete = True ;


CREATE VIEW vAuteur AS
        SELECT * FROM Contributeur WHERE auteur = True ;


CREATE VIEW vRealisteur AS
        SELECT * FROM Contributeur WHERE realisateur = True;


CREATE VIEW vActeur AS
        SELECT * FROM Contributeur WHERE acteur = True ;




CREATE VIEW vLivre AS
        SELECT * FROM Ressource JOIN Livre ON Ressource.code = Livre.codeLivre;


 CREATE VIEW vOeuvreMusicale AS
        SELECT * FROM Ressource JOIN OeuvreMusicale ON Ressource.code = OeuvreMusicale.codeMusique;


CREATE VIEW vFilm AS
        SELECT * FROM Ressource JOIN Film ON Ressource.code = Film.codeFilm;