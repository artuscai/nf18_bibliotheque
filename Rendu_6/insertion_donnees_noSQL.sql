-- Insertion de données fictives dans la table Contributeur



INSERT INTO Contributeur (id, nom, prenom, dateNaissance, nationalite, compositeur, interprete, auteur, realisateur, acteur)
VALUES
(1, 'Beethoven', 'Ludwig', '1770-12-17', 'Allemand', TRUE, FALSE, FALSE, FALSE, FALSE),
(2, 'Nolan', 'Christopher', '1970-07-30', 'Américain', FALSE, FALSE, FALSE, TRUE, FALSE),
(3, 'Verne', 'Jules', '1828-02-08', 'Français', FALSE, FALSE, TRUE, FALSE, FALSE),
(4, 'Mozart', 'Wolfgang', '1756-01-27', 'Autrichien', TRUE, TRUE, FALSE, FALSE, FALSE),
(5, 'Spielberg', 'Steven', '1946-12-18', 'Américain', FALSE, FALSE, FALSE, TRUE, FALSE),
(6, 'Piaf', 'Édith', '1915-12-19', 'Française', FALSE, TRUE, FALSE, FALSE, FALSE),
(7, 'Chaplin', 'Charlie', '1889-04-16', 'Britannique', FALSE, FALSE, FALSE, TRUE, TRUE),
(8, 'Rowling', 'J.K.', '1965-07-31', 'Britannique', FALSE, FALSE, TRUE, FALSE, FALSE),
(9, 'Gaga', 'Lady', '1986-03-28', 'Américaine', TRUE, TRUE, FALSE, FALSE, TRUE),
(10, 'Tarantino', 'Quentin', '1963-03-27', 'Américain', FALSE, FALSE, FALSE, TRUE, TRUE);


-- Insertion de données fictives dans la table Ressource
INSERT INTO Ressource (code, titre, dateApparition, editeur, genre, codeClassification)
VALUES
(1, 'Symphonie No. 5', '1808-12-22', 'Inconnu', 'Classique', 'MUS123'),
(2, 'Inception', '2010-07-16', 'Warner Bros.', 'Science-fiction', 'FILM456'),
(3, 'Vingt mille lieues sous les mers', '1870-01-01', 'Pierre-Jules Hetzel', 'Aventure', 'BOOK789'),
(4, 'Requiem', '1791-12-05', 'Nannerl Publishing', 'Classique', 'MUS321'),
(5, 'La Liste de Schindler', '1993-11-30', 'Universal Pictures', 'Drame', 'FILM654'),
(6, 'La Vie en Rose', '1956-02-15', 'Columbia Records', 'Chanson', 'MUS741'),
(7, 'Les Temps Modernes', '1936-02-25', 'United Artists', 'Comédie', 'FILM852'),
(8, 'Harry Potter à l’école des sorciers', '1997-06-26', 'Bloomsbury', 'Fantaisie', 'BOOK963');


-- Insertion de données fictives dans la table Livre
-- Ajout de livres avec des codes et des ISBN correspondants
INSERT INTO Livre (codeLivre, ISBN, resume, langue)
VALUES
(3, '978-0140624378', 'Un roman de science-fiction classique sur le sous-marin Nautilus.', 'Français'),
(8, '978-0747532699', 'Le premier livre de la série Harry Potter, introduisant le jeune sorcier.', 'Anglais');


-- Insertion de données fictives dans la table OeuvreMusicale
-- Ajout de compositions musicales avec des durées et des styles
INSERT INTO OeuvreMusicale (codeMusique, longueur, style)
VALUES
(1, 450, 'Symphonie'),
(4, 3600, 'Requiem'),
(6, 210, 'Chanson');


-- Insertion de données fictives dans la table Film
-- Ajout de films avec leur langue, durée et synopsis
INSERT INTO Film (codeFilm, langue, longueur, synopsis)
VALUES
(2, 'Anglais', 148, 'Un voleur qui utilise la technologie de partage de rêves pour voler des secrets d’entreprise.'),
(5, 'Anglais', 195, 'Un film sur les efforts humanitaires d’Oskar Schindler pendant la Seconde Guerre mondiale.'),
(7, 'Anglais', 87, 'Une comédie qui satirise l’ère industrielle.');


-- Insertion de données fictives dans la table ContribueLivre
-- Attribution des contributions d'auteurs à des livres
INSERT INTO ContribueLivre (idContributeur, codeLivre)
VALUES
(3, 3), -- Jules Verne contribue au livre "Vingt mille lieues sous les mers".
(8, 8); -- J.K. Rowling contribue au livre "Harry Potter à l’école des sorciers".


-- Insertion de données fictives dans la table ContribueFilm
-- Attribution des contributions de réalisateurs et d'acteurs aux films
INSERT INTO ContribueFilm (idContributeur, codeFilm)
VALUES
(2, 2), -- Christopher Nolan contribue au film "Inception" en tant que réalisateur.
(5, 5), -- Steven Spielberg contribue au film "La Liste de Schindler".
(7, 7), -- Charlie Chaplin contribue au film "Les Temps Modernes".
(10, 5); -- Quentin Tarantino contribue au film "La Liste de Schindler".


-- Insertion de données fictives dans la table ContribueMusique
-- Attribution des contributions de compositeurs et interprètes aux œuvres musicales
INSERT INTO ContribueMusique (idContributeur, codeMusic)
VALUES
(1, 1), -- Beethoven contribue à la "Symphonie No. 5".
(4, 4), -- Mozart contribue au "Requiem".
(6, 6), -- Édith Piaf contribue à "La Vie en Rose".
(9, 6); -- Lady Gaga contribue également en tant qu'interprète.




-- Insertion de données fictives dans la table Exemplaire
-- Ajout d'exemplaires de ressources avec l'état précisé
INSERT INTO Exemplaire (code, id, etat)
VALUES
(1, 1, 'neuf'), -- Exemplaire de la Symphonie No. 5
(1, 2, 'bon'), -- Deuxième exemplaire de la Symphonie No. 5
(3, 1, 'neuf'), -- Exemplaire du livre "Vingt mille lieues sous les mers"
(8, 1, 'bon'), -- Exemplaire du livre "Harry Potter à l’école des sorciers"
(2, 1, 'neuf'), -- Exemplaire du film "Inception"
(5, 1, 'bon'), -- Exemplaire du film "La Liste de Schindler"
(7, 1, 'abime'), -- Exemplaire du film "Les Temps Modernes"
(6, 1, 'neuf'); -- Exemplaire de la chanson "La Vie en Rose"


-- Insertion de données fictives dans la table Adherent
-- Insert adherents with dynamically generated IDs for JSON objects


-- Insert adherents, dynamically generating unique IDs for JSON objects
INSERT INTO Adherent_JSON (id, nom, prenom, email, blackliste, compteutilisateur, sanctions, prets)
VALUES
(1, 'Dupont', 'Jean', 'jean.dupont@example.com', FALSE,
    '{"login": "jeandupont", "password": "password123"}',
    jsonb_build_array(jsonb_build_object(
        'idSanction', nextval('json_object_id_seq'),
        'dateDebut', '2023-06-15',
        'duree', 15,
        'typeSanction', 'degradation'
    )),
    jsonb_build_array(jsonb_build_object(
        'code', 1,
        'idExemplaire', 1,
        'datePret', '2023-09-01',
        'duree', 14,
        'dateRendu', '2023-09-15'
    ))
),
(2, 'Martin', 'Sophie', 'sophie.martin@example.com', FALSE,
    '{"login": "sophiemartin", "password": "sophiePass"}',
    '[]'::jsonb,
    jsonb_build_array(jsonb_build_object(
        'code', 3,
        'idExemplaire', 1,
        'datePret', '2024-09-05',
        'duree', 21,
        'dateRendu', NULL
    ))
),
(3, 'Bernard', 'Luc', 'luc.bernard@example.com', TRUE,
    '{"login": "lucbernard", "password": "bernard123"}',
    jsonb_build_array(jsonb_build_object(
        'idSanction', nextval('json_object_id_seq'),
        'dateDebut', '2023-05-01',
        'duree', 30,
        'typeSanction', 'retard'
    )),
    '[]'::jsonb
),
(4, 'Durand', 'Marie', 'marie.durand@example.com', FALSE,
    NULL,
    '[]'::jsonb,
    jsonb_build_array(jsonb_build_object(
        'code', 2,
        'idExemplaire', 1,
        'datePret', '2023-09-10',
        'duree', 7,
        'dateRendu', NULL
    ))
),
(5, 'Lemoine', 'Pierre', 'pierre.lemoine@example.com', FALSE,
    NULL,
    '[]'::jsonb,
    jsonb_build_array(jsonb_build_object(
        'code', 8,
        'idExemplaire', 1,
        'datePret', '2023-09-15',
        'duree', 30,
        'dateRendu', '2024-01-01'
    ))
),
(6, 'Garcia', 'Elena', 'elena.garcia@example.com', FALSE,
    '{"login": "elenagarcia", "password": "elenaPass"}',
    jsonb_build_array(jsonb_build_object(
        'idSanction', nextval('json_object_id_seq'),
        'dateDebut', '2024-01-15',
        'duree', 7,
        'typeSanction', 'retard'
    )),
    jsonb_build_array(jsonb_build_object(
        'code', 9,
        'idExemplaire', 1,
        'datePret', '2024-02-01',
        'duree', 14,
        'dateRendu', NULL
    ))
),
(7, 'Smith', 'John', 'john.smith@example.com', FALSE,
    '{"login": "johnsmith", "password": "john123"}',
    '[]'::jsonb,
    jsonb_build_array(jsonb_build_object(
        'code', 13,
        'idExemplaire', 1,
        'datePret', '2024-02-10',
        'duree', 30,
        'dateRendu', NULL
    ))
);

-- Ajout de membres du personnel de la bibliothèque
INSERT INTO MembrePersonnel_JSON (id, nom, prenom, adresse, email, compteutilisateur)
VALUES
(1, 'Roux', 'Camille', '10 rue des Lilas, Paris', 'camille.roux@example.com', '{"login": "camilleroux", "password": "admin123"}'),
(2, 'Leroy', 'Thomas', '15 avenue des Champs, Lyon', 'thomas.leroy@example.com', '{"login": "thomasleroy", "password": "staff123"}'),
(3, 'Blanc', 'Marie', '20 rue des Rosiers, Bordeaux', 'marie.blanc@example.com', '{"login": "marieblanc", "password": "staff456"}');



-- Insertion de données fictives dans la table Contributeur
INSERT INTO Contributeur (id, nom, prenom, dateNaissance, nationalite, compositeur, interprete, auteur, realisateur, acteur)
VALUES
(11, 'Tchaikovsky', 'Pyotr', '1840-05-07', 'Russe', TRUE, FALSE, FALSE, FALSE, FALSE),
(12, 'Jackson', 'Michael', '1958-08-29', 'Américain', TRUE, TRUE, FALSE, FALSE, TRUE),
(13, 'Hitchcock', 'Alfred', '1899-08-13', 'Britannique', FALSE, FALSE, FALSE, TRUE, FALSE),
(14, 'Shakespeare', 'William', '1564-04-23', 'Britannique', FALSE, FALSE, TRUE, FALSE, FALSE),
(15, 'Kubrick', 'Stanley', '1928-07-26', 'Américain', FALSE, FALSE, FALSE, TRUE, FALSE);

-- Insertion de données fictives dans la table Ressource
INSERT INTO Ressource (code, titre, dateApparition, editeur, genre, codeClassification)
VALUES
(9, 'Le Lac des cygnes', '1876-03-04', 'The Imperial Ballet', 'Classique', 'MUS555'),
(10, 'Thriller', '1982-11-30', 'Epic Records', 'Pop', 'MUS777'),
(11, 'Psycho', '1960-06-16', 'Paramount Pictures', 'Horreur', 'FILM123'),
(12, 'Hamlet', '1603-01-01', 'The Globe Theatre', 'Théâtre', 'BOOK111'),
(13, '2001: A Space Odyssey', '1968-04-02', 'MGM', 'Science-fiction', 'FILM999');

-- Insertion de données fictives dans la table Livre
INSERT INTO Livre (codeLivre, ISBN, resume, langue)
VALUES
(12, '978-0486272788', 'Une tragédie dramatique autour de la vengeance et de la folie.', 'Anglais');

-- Insertion de données fictives dans la table OeuvreMusicale
INSERT INTO OeuvreMusicale (codeMusique, longueur, style)
VALUES
(9, 1800, 'Ballet'),
(10, 420, 'Pop');

-- Insertion de données fictives dans la table Film
INSERT INTO Film (codeFilm, langue, longueur, synopsis)
VALUES
(11, 'Anglais', 109, 'Un film d’horreur psychologique autour d’un motel isolé.'),
(13, 'Anglais', 149, 'Un voyage interstellaire explorant l’évolution humaine.');

-- Insertion de données fictives dans la table ContribueLivre
INSERT INTO ContribueLivre (idContributeur, codeLivre)
VALUES
(14, 12); -- William Shakespeare wrote "Hamlet".

-- Insertion de données fictives dans la table ContribueFilm
INSERT INTO ContribueFilm (idContributeur, codeFilm)
VALUES
(13, 11), -- Alfred Hitchcock directed "Psycho".
(15, 13); -- Stanley Kubrick directed "2001: A Space Odyssey".

-- Insertion de données fictives dans la table ContribueMusique
INSERT INTO ContribueMusique (idContributeur, codeMusic)
VALUES
(11, 9), -- Tchaikovsky composed "Le Lac des cygnes".
(12, 10); -- Michael Jackson performed "Thriller".

-- Insertion de données fictives dans la table Exemplaire
INSERT INTO Exemplaire (code, id, etat)
VALUES
(9, 1, 'neuf'), -- Exemplaire de "Le Lac des cygnes".
(10, 1, 'bon'), -- Exemplaire de "Thriller".
(11, 1, 'abime'), -- Exemplaire de "Psycho".
(12, 1, 'neuf'), -- Exemplaire de "Hamlet".
(13, 1, 'bon'); -- Exemplaire de "2001: A Space Odyssey".




-- Insertion de données fictives supplémentaires dans la table Exemplaire
INSERT INTO Exemplaire (code, id, etat)
VALUES
(4, 1, 'neuf'), -- Exemplaire du "Requiem".
(4, 2, 'bon'), -- Deuxième exemplaire du "Requiem".
(5, 2, 'neuf'), -- Deuxième exemplaire de "La Liste de Schindler".
(8, 2, 'abime'), -- Deuxième exemplaire de "Harry Potter à l’école des sorciers".
(7, 2, 'bon'), -- Deuxième exemplaire de "Les Temps Modernes".
(6, 2, 'bon'), -- Deuxième exemplaire de "La Vie en Rose".
(3, 2, 'bon'), -- Deuxième exemplaire de "Vingt mille lieues sous les mers".
(2, 2, 'neuf'); -- Deuxième exemplaire de "Inception".