# I - Note de clarification

## Rôles

### Pour les adhérents:

- Rechercher des ressources avec des mots clés
- Gérer leurs emprunts (voir les dates limites pour rendre, etc.)


**Accès**
  - Accès aux ressources, contributeurs et exemplaires en lecture seule
  - Accès à son compte utilisateur
  - Accès aux prêts, et sanctions qui lui correspondent en lecture seule

### Pour le personnel:

- Ajouter, supprimer, modifier des ressources et exemplaires
- Gestion des prêts, des retards et des réservations
- Faire des statistiques sur les documents empruntés par les adhérents
- Blacklister des adhérents
- Gestion des sanctions

**Accès**
  - Accès aux ressources, contributeurs et exemplaires en lecture et écriture
  - Accès aux adhérants, prêts, et sanctions en lecture et écriture

## Définition des objets et des relations

### Ressource:

- **Attributs** : `code` (clé), `titre`, `date d’apparition`, `éditeur`, `genre`, `code de classification`
- **Note** : Ressource est une classe abstraite

### Livre (est une ressource):

- **Attributs** : `ISBN` (clé), `résumé`, `langue`
- Un livre a des **auteurs** (qui sont des contributeurs) (n - n)

### Film (est une ressource):

- **Attributs** : `langue`, `longueur`, `synopsis`
- Un film a des **réalisateurs** et des **acteurs** (n - n)

### Œuvre musicale (est une ressource):

- **Attributs** : `longueur`, `style`
- Une œuvre musicale a des **compositeurs** et des **interprètes** (n - n)

### Exemplaire

- **Attributs** : `état` (neuf, bon, abîmé, perdu)
- Un exemplaire est complètement dépendant de sa ressource ( composition )

### Contributeur:

- **Attributs** : `nom`, `prénom`, `date de naissance`, `nationalité`

- **Sous-classes** :
  - **Auteur** (est un contributeur)
  - **Compositeur** (est un contributeur)
  - **Interprète** (est un contributeur)
  - **Réalisateur** (est un contributeur)
  - **Acteur** (est un contributeur)

### Compte utilisateur:

- **Attributs** : `login` (clé), `mot de passe`

### Membre du personnel:

- **Attributs** : `nom`, `prénom`, `adresse`, `adresse e-mail`
- Un membre du personnel a un **compte utilisateur** (1 – 1)

### Adhérent:

- **Attributs** : `nom`, `prénom`, `date de naissance`, `adresse`, `adresse e-mail`, `numéro de téléphone`, `blacklisté` (bool)
- On souhaite garder une trace des anciens adhérents
- Seul les adhérents **actifs** ont un **compte utilisateur** (1 – 0..1)

### Prêt (classe d’association entre Exemplaire et Adhérent):

- **Attributs** : `date de prêt`, `durée de prêt`, `dateRendu`
- Un adhérant peux emprunter des documents
 - Un document ne peut être emprunté que s'il est disponible et en bon état
 - Un adhérent ne peut emprunter simultanément qu'un nombre limité d'œuvres (nous avons choisi 4 arbitrairement), chacune pour une durée limitée.
 - Relation (0..4 - 0..1) entre Exemplaire et Adhérant. 
 ### Sanction
 - **Attributs** : `dateDebut`, `durée`, `typeSanction` (retard, dégradation, perte)
 - Une sanction concerne 1 adhérant, un adhérant peut avoir plusieurs sanctions 
  - Relation (n - 1) entre Sanction et Adhérant

## Contraintes complexes à prévoir:

- Un adhérent doit s'identifier avant de pouvoir emprunter
- Tout retard dans la restitution des documents empruntés entraîne une suspension du droit de prêt d'une durée égale au nombre de jours de retard
- En cas de perte ou détérioration grave d'un document, la suspension du droit de prêt est maintenue jusqu'à ce que l'adhérent rembourse le document
